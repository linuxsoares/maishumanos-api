from django.shortcuts import render
from rest_framework import viewsets

from gamification.models import GamificationUser
from gamification.serializer import GamificationUserSerializer


class GamificationUserViewSet(viewsets.ModelViewSet):
    queryset = GamificationUser.objects.all()
    serializer_class = GamificationUserSerializer
