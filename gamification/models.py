from django.db import models

from account.models import User
from hemocenter.models import Hemocenter

# Create your models here.

class GamificationUser(models.Model):
    account = models.ForeignKey(User, on_delete=models.PROTECT)
    point = models.IntegerField(null=False, default=0)
    hemocenter = models.ForeignKey(Hemocenter, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
