from rest_framework import serializers

from gamification.models import GamificationUser


class GamificationUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GamificationUser
        fields = '__all__'
