from django.shortcuts import render
from rest_framework import viewsets

from hemocenter.models import Hemocenter
from hemocenter.serializer import HemocenterSerializer


class HemocenterViewSet(viewsets.ModelViewSet):
    queryset = Hemocenter.objects.all()
    serializer_class = HemocenterSerializer
