from django.apps import AppConfig


class HemocenterConfig(AppConfig):
    name = 'hemocenter'
