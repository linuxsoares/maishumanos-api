from rest_framework import serializers

from hemocenter.models import Hemocenter
from core.serializer import AddressSerializer


class HemocenterSerializer(serializers.HyperlinkedModelSerializer):
    address = AddressSerializer()
    
    class Meta:
        model = Hemocenter
        fields = '__all__'
