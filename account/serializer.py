from rest_framework import serializers
from core.models import Address

from account.models import User


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        exclude = ('url',)


class UserSerializer(serializers.ModelSerializer):
    address = AddressSerializer()

    class Meta:
        model = User
        fields = (
            'password',
            'username',
            'first_name',
            'last_name',
            'email',
            'blood_type',
            'age',
            'address',
        )


