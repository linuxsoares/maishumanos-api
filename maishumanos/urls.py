from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework.authtoken import views as rest_framework_views
from rest_framework_swagger.views import get_swagger_view

from account.views import UserViewSet
from gamification.views import GamificationUserViewSet
from hemocenter.views import HemocenterViewSet

router = routers.SimpleRouter()
router.register(r'accounts', UserViewSet)
router.register(r'gamification', GamificationUserViewSet)
router.register(r'hemocenter', HemocenterViewSet)


schema_view = get_swagger_view(title='+Humanos API')

urlpatterns = [
    path('docs/', schema_view, name='docs'),
    path('admin/', admin.site.urls, name='admin'),
    path('api-auth/', include('rest_framework.urls'), name='api-auth'),
    path(
        'get_auth_token/',
        rest_framework_views.obtain_auth_token,
        name='get_auth_token'
    ),
]
urlpatterns += router.urls
