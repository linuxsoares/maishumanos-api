# maishumanos

API Para aplicativo de doação de sangue +HUMANOS.

## Installation

Create a virtualenv (example created with `virtualenvwrapper`):

    mkvirtualenv maishumanos

Install requirements:

    make dependencies

Create database tables:

    make migrate

Run the project:

    make run

## Tests

To run the test suite, execute:

    make test
