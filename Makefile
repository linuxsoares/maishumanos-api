MANAGE_PY=manage.py
SETTINGS_DEVELOPMENT=maishumanos.settings.development

clean:
	@find . -name "*.pyc" | xargs rm -rf
	@find . -name "*.pyo" | xargs rm -rf
	@find . -name "__pycache__" -type d | xargs rm -rf
	@rm -f .coverage
	@rm -rf htmlcov/
	@rm -f coverage.xml
	@rm -f *.log

shell:
	@python $(MANAGE_PY) shell --settings=$(SETTINGS_DEVELOPMENT)

run:
	./manage.py runserver --settings=$(SETTINGS_DEVELOPMENT)

test: clean
	@py.test -x -p no:sugar .

flake8:
	@flake8 --show-source .

check-python-import:
	@isort --check

fix-python-import:
	@isort -rc .

migration:
	./manage.py makemigrations --settings=$(SETTINGS_DEVELOPMENT)

migrate:
	./manage.py migrate --settings=$(SETTINGS_DEVELOPMENT)

outdated:
	@pip list --outdated --format=columns

dependencies:
	@pip install -U -r maishumanos/requirements/base.txt